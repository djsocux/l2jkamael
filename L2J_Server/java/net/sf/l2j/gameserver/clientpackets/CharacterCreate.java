/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.clientpackets;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.datatables.CharNameTable;
import net.sf.l2j.gameserver.datatables.CharTemplateTable;
import net.sf.l2j.gameserver.datatables.SkillTable;
import net.sf.l2j.gameserver.datatables.SkillTreeTable;
import net.sf.l2j.gameserver.idfactory.IdFactory;
import net.sf.l2j.gameserver.instancemanager.QuestManager;
import net.sf.l2j.gameserver.model.L2ItemInstance;
import net.sf.l2j.gameserver.model.L2ShortCut;
import net.sf.l2j.gameserver.model.L2SkillLearn;
import net.sf.l2j.gameserver.model.L2World;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
import net.sf.l2j.gameserver.model.base.Race;
import net.sf.l2j.gameserver.model.quest.Quest;
import net.sf.l2j.gameserver.model.quest.QuestState;
import net.sf.l2j.gameserver.network.L2GameClient;
import net.sf.l2j.gameserver.serverpackets.CharCreateFail;
import net.sf.l2j.gameserver.serverpackets.CharCreateOk;
import net.sf.l2j.gameserver.serverpackets.CharSelectionInfo;
import net.sf.l2j.gameserver.templates.L2Item;
import net.sf.l2j.gameserver.templates.L2PcTemplate;
import net.sf.l2j.gameserver.util.Util;

/**
 * This class ...
 *
 * @version $Revision: 1.9.2.3.2.8 $ $Date: 2005/03/27 15:29:30 $
 */
@SuppressWarnings("unused")
public final class CharacterCreate extends L2GameClientPacket
{
	private static final String _C__0B_CHARACTERCREATE = "[C] 0B CharacterCreate";
	private static Logger _log = Logger.getLogger(CharacterCreate.class.getName());

	// cSdddddddddddd
	private String _name;
    private int _race;
	private byte _sex;
	private int _classId;
	private int _int;
	private int _str;
	private int _con;
	private int _men;
	private int _dex;
	private int _wit;
	private byte _hairStyle;
	private byte _hairColor;
	private byte _face;

	@Override
	protected void readImpl()
	{
		_name      = readS();
		_race      = readD();
		_sex       = (byte)readD();
		_classId   = readD();
		_int       = readD();
		_str       = readD();
		_con       = readD();
		_men       = readD();
		_dex       = readD();
		_wit       = readD();
		_hairStyle = (byte)readD();
		_hairColor = (byte)readD();
		_face      = (byte)readD();
	}

	@Override
	protected void runImpl()
	{
        if (CharNameTable.getInstance().accountCharNumber(getClient().getAccountName()) >= Config.MAX_CHARACTERS_NUMBER_PER_ACCOUNT && Config.MAX_CHARACTERS_NUMBER_PER_ACCOUNT != 0)
        {
            if (Config.DEBUG)
                _log.fine("Max number of characters reached. Creation failed.");
            CharCreateFail ccf = new CharCreateFail(CharCreateFail.REASON_TOO_MANY_CHARACTERS);
            sendPacket(ccf);
            return;
        }
        else if (CharNameTable.getInstance().doesCharNameExist(_name))
		{
			if (Config.DEBUG)
				_log.fine("charname: "+ _name + " already exists. creation failed.");
			CharCreateFail ccf = new CharCreateFail(CharCreateFail.REASON_NAME_ALREADY_EXISTS);
			sendPacket(ccf);
			return;
		}
		else if ((_name.length() < 3) || (_name.length() > 16) || !Util.isAlphaNumeric(_name) || !isValidName(_name))
		{
			if (Config.DEBUG)
				_log.fine("charname: " + _name + " is invalid. creation failed.");
			CharCreateFail ccf = new CharCreateFail(CharCreateFail.REASON_16_ENG_CHARS);
			sendPacket(ccf);
			return;
		}

		
		
		L2PcTemplate template = CharTemplateTable.getInstance().getTemplate(_classId);
		
        if (Config.DEBUG)
            _log.fine("charname: " + _name + " classId: " + _classId+" template: "+template);
        
        if(template == null || template.classBaseLevel > 1) 
		{
			CharCreateFail ccf = new CharCreateFail(CharCreateFail.REASON_CREATION_FAILED);
			sendPacket(ccf);
			return;
		}

		int objectId = IdFactory.getInstance().getNextId();
		L2PcInstance newChar = L2PcInstance.create(objectId, template, getClient().getAccountName(),
				_name, _hairStyle, _hairColor, _face, _sex!=0);
		newChar.setCurrentHp(template.baseHpMax);
		newChar.setCurrentCp(template.baseCpMax);
		newChar.setCurrentMp(template.baseMpMax);
		//newChar.setMaxLoad(template.baseLoad);

		// send acknowledgement
		CharCreateOk cco = new CharCreateOk();
		sendPacket(cco);

		initNewChar(getClient(), newChar);
	}

    private boolean isValidName(String text)
    {
            boolean result = true;
            String test = text;
            Pattern pattern;
            try
            {
                pattern = Pattern.compile(Config.CNAME_TEMPLATE);
            }
            catch (PatternSyntaxException e) // case of illegal pattern
            {
            	_log.warning("ERROR : Character name pattern of config is wrong!");
                pattern = Pattern.compile(".*");
            }
            Matcher regexp = pattern.matcher(test);
            if (!regexp.matches())
            {
                    result = false;
            }
            return result;
    }

	private void initNewChar(L2GameClient client, L2PcInstance newChar)
	{
		if (Config.DEBUG) _log.fine("Character init start");
		L2World.getInstance().storeObject(newChar);

		L2PcTemplate template = newChar.getTemplate();

		newChar.addAdena("Init", Config.STARTING_ADENA, null, false);

		newChar.setXYZInvisible(template.spawnX, template.spawnY, template.spawnZ);
		newChar.setTitle("");

		L2ShortCut shortcut;
		//add attack shortcut
		shortcut = new L2ShortCut(0,0,3,2,-1,1);
		newChar.registerShortCut(shortcut);
		//add take shortcut
		shortcut = new L2ShortCut(3,0,3,5,-1,1);
		newChar.registerShortCut(shortcut);
		//add sit shortcut
		shortcut = new L2ShortCut(10,0,3,0,-1,1);
		newChar.registerShortCut(shortcut);
		
		               // by iSparky
		               if (Config.ENABLE_HUMAN_REWARD)
		               {
		                       if (newChar.getRace() == Race.Human || newChar.isMageClass())
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.HUMAN_M_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.HUMAN_M_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.HUMAN_M_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.HUMAN_M_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.HUMAN_M_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.HUMAN_M_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		                       else
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.HUMAN_F_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.HUMAN_F_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.HUMAN_F_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.HUMAN_F_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.HUMAN_F_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.HUMAN_F_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		               }
		              
		               if (Config.ENABLE_ELF_REWARD)
		               {
		                       if (newChar.getRace() == Race.Elf || newChar.isMageClass())
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.ELF_M_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.ELF_M_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.ELF_M_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.ELF_M_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.ELF_M_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.ELF_M_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		                       else
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.ELF_F_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.ELF_F_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.ELF_F_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.ELF_F_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.ELF_F_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.ELF_F_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		               }
		              
		               if (Config.ENABLE_DE_REWARD)
		               {
		                       if (newChar.getRace() == Race.DarkElf || newChar.isMageClass())
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.DE_M_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.DE_M_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.DE_M_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.DE_M_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.DE_M_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.DE_M_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		                       else
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.DE_F_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.DE_F_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.DE_F_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.DE_F_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.DE_F_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.DE_F_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		               }
		              
		               if (Config.ENABLE_ORC_REWARD)
		               {
		                       if (newChar.getRace() == Race.Orc || newChar.isMageClass())
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.ORC_M_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.ORC_M_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.ORC_M_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.ORC_M_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.ORC_M_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.ORC_M_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		                       else
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.ORC_F_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.ORC_F_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.ORC_F_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.ORC_F_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.ORC_F_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.ORC_F_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		               }
		              
		               if (Config.ENABLE_DWARF_REWARD)
		               {
		                       if (newChar.getRace() == Race.Dwarf)
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.DWARF_F_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.DWARF_F_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.DWARF_F_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.DWARF_F_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.DWARF_F_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.DWARF_F_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		               }
		               
		               if (Config.ENABLE_KAMAEL_REWARD)
		               {
		                       if (newChar.getRace() == Race.Kamael)
		                       {
		                               final L2ItemInstance Helm = newChar.getInventory().addItem("Init", Config.KAMAEL_F_HELM_ID, 1, newChar, null);
		                               if (Helm.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Helm.getItem().getType2() != L2Item.SLOT_HEAD))
		                                               newChar.getInventory().equipItemAndRecord(Helm);
		                               final L2ItemInstance Chest = newChar.getInventory().addItem("Init", Config.KAMAEL_F_CHEST_ID, 1, newChar, null);
		                               if (Chest.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Chest.getItem().getType2() != L2Item.SLOT_CHEST))
		                                               newChar.getInventory().equipItemAndRecord(Chest);
		                               final L2ItemInstance Legs = newChar.getInventory().addItem("Init", Config.KAMAEL_F_LEGS_ID, 1, newChar, null);
		                               if (Legs.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Legs.getItem().getType2() != L2Item.SLOT_LEGS))
		                                               newChar.getInventory().equipItemAndRecord(Legs);
		                               final L2ItemInstance Boots = newChar.getInventory().addItem("Init", Config.KAMAEL_F_BOOTS_ID, 1, newChar, null);
		                               if (Boots.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Boots.getItem().getType2() != L2Item.SLOT_FEET))
		                                               newChar.getInventory().equipItemAndRecord(Boots);
		                               final L2ItemInstance Gloves = newChar.getInventory().addItem("Init", Config.KAMAEL_F_GLOVES_ID, 1, newChar, null);
		                               if (Gloves.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Gloves.getItem().getType2() != L2Item.SLOT_GLOVES))
		                                               newChar.getInventory().equipItemAndRecord(Gloves);
		                               final L2ItemInstance Weap = newChar.getInventory().addItem("Init", Config.KAMAEL_F_WEAPON_ID, 1, newChar, null);
		                               if (Weap.isEquipable())
		                                       if (newChar.getActiveWeaponItem() == null || !(Weap.getItem().getType2() != L2Item.TYPE2_WEAPON))
		                                               newChar.getInventory().equipItemAndRecord(Weap);
		                       }
		               }
		
		L2Item[] items = template.getItems();
		for (int i = 0; i < items.length; i++)
		{
			L2ItemInstance item = newChar.getInventory().addItem("Init", items[i].getItemId(), 1, newChar, null);
			if (item.getItemId()==5588){
			    //add tutbook shortcut
			    shortcut = new L2ShortCut(11,0,1,item.getObjectId(),-1,1);
			    newChar.registerShortCut(shortcut);
			}
			if (item.isEquipable()){
			  if (newChar.getActiveWeaponItem() == null || !(item.getItem().getType2() != L2Item.TYPE2_WEAPON))
			    newChar.getInventory().equipItemAndRecord(item);
			}
		}

		L2SkillLearn[] startSkills = SkillTreeTable.getInstance().getAvailableSkills(newChar, newChar.getClassId());
		for (int i = 0; i < startSkills.length; i++)
		{
			newChar.addSkill(SkillTable.getInstance().getInfo(startSkills[i].getId(), startSkills[i].getLevel()), true);
			if (startSkills[i].getId()==1001 || startSkills[i].getId()==1177){
			    shortcut = new L2ShortCut(1,0,2,startSkills[i].getId(),1,1);
			    newChar.registerShortCut(shortcut);
			}
			if (startSkills[i].getId()==1216){
			    shortcut = new L2ShortCut(10,0,2,startSkills[i].getId(),1,1);
			    newChar.registerShortCut(shortcut);
			}
			if (Config.DEBUG)
				_log.fine("adding starter skill:" + startSkills[i].getId()+ " / "+ startSkills[i].getLevel());
		}
		startTutorialQuest(newChar);
		L2GameClient.saveCharToDisk(newChar);
		newChar.deleteMe(); // release the world of this character and it's inventory

		// send char list

		CharSelectionInfo cl =	new CharSelectionInfo(client.getAccountName(), client.getSessionId().playOkID1);
		client.getConnection().sendPacket(cl);
        client.setCharSelection(cl.getCharInfo());
        if (Config.DEBUG) _log.fine("Character init end");
	}

	public void startTutorialQuest(L2PcInstance player)
	{
		QuestState qs = player.getQuestState("255_Tutorial");
		Quest q = null;
		if (qs == null)
			q = QuestManager.getInstance().getQuest("255_Tutorial");
		if (q != null)
			q.newQuestState(player);
	}

	/* (non-Javadoc)
	 * @see net.sf.l2j.gameserver.clientpackets.ClientBasePacket#getType()
	 */
	@Override
    public String getType()
	{
		return _C__0B_CHARACTERCREATE;
	}
}
